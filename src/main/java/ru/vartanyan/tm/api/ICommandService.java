package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
