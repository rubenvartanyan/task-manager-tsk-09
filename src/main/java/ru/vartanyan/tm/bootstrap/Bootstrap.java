package ru.vartanyan.tm.bootstrap;

import ru.vartanyan.tm.api.ICommandController;
import ru.vartanyan.tm.api.ICommandRepository;
import ru.vartanyan.tm.api.ICommandService;
import ru.vartanyan.tm.constant.ArgumentConstant;
import ru.vartanyan.tm.constant.TerminalConstant;
import ru.vartanyan.tm.controller.CommandController;
import ru.vartanyan.tm.repository.CommandRepository;
import ru.vartanyan.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String... args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConstant.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConstant.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConstant.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConstant.ARG_INFO: commandController.showInfo(); break;
            default: showIncorrectArg();
        }
    }


    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(final String arg) {
        if (arg == null) return;
        switch (arg){
            case TerminalConstant.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConstant.CMD_HELP: commandController.showHelp(); break;
            case TerminalConstant.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConstant.CMD_INFO: commandController.showInfo(); break;
            case TerminalConstant.CMD_EXIT: commandController.exit(); break;
            case TerminalConstant.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConstant.CMD_ARGUMENTS: commandController.showArguments(); break;
            default: showIncorrectCommand();
        }
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }


}
