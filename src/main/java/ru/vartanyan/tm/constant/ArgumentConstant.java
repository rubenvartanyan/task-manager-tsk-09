package ru.vartanyan.tm.constant;

public interface ArgumentConstant {

    String ARG_HELP = "-h";
    String ARG_VERSION = "-v";
    String ARG_ABOUT = "-a";
    String ARG_INFO = "-i";

}
